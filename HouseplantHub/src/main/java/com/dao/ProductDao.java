package com.dao;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
//import org.springframework.context.annotation.Configuration;
import org.springframework.stereotype.Service;

import com.model.Product;

@Service
public class ProductDao {
	
	@Autowired
	ProductRepository productRepository;
	
	public List<Product> getProducts() {
		return productRepository.findAll();
	}
	
	public Product getProductById(int productId) {
		return productRepository.findById(productId);
	}
	
//	public Product getProductByName(String productName) {
//		return productRepository.findByName(productName);
//	}
	
	public void registerProduct(Product product) {
		productRepository.save(product);
	}
	
	public void updateProduct(Product product) {
		productRepository.save(product);
	}
	
	public void deleteProductById(int productId) {
		productRepository.deleteById(productId);
	}

}