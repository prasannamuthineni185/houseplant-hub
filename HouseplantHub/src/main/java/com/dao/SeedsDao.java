//package com.dao;
//
//import java.util.List;
//
//import org.springframework.beans.factory.annotation.Autowired;
////import org.springframework.context.annotation.Configuration;
//import org.springframework.stereotype.Service;
//
//import com.model.Seeds;
//
//@Service
//public class SeedsDao {
//	
//	@Autowired
//	SeedsRepository SeedsRepository;
//	
//	public List<Seeds> getSeeds() {
//		return SeedsRepository.findAll();
//	}
//	
//	public Seeds getSeedsById(int SeedsId) {
//		return SeedsRepository.findById(SeedsId).orElse(new Seeds());
//	}
//	
//	public Seeds getSeedsByName(String SeedsName) {
//		return SeedsRepository.findByName(SeedsName);
//	}
//	
//	public void registerSeeds(Seeds Seeds) {
//		SeedsRepository.save(Seeds);
//	}
//	
//	public void updateSeeds(Seeds Seeds) {
//		SeedsRepository.save(Seeds);
//	}
//	
//	public void deleteSeedsById(int SeedsId) {
//		SeedsRepository.deleteById(SeedsId);
//	}
//
//}