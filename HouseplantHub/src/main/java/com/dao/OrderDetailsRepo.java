package com.dao;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import com.model.CartQ;
import com.model.OrderDetails;

public interface OrderDetailsRepo extends JpaRepository<OrderDetails,Integer>{
	
	@Query("from OrderDetails  c where c.emailId = :emailId ")
	List<OrderDetails> getByEmailId(@Param("emailId") String email);
}
