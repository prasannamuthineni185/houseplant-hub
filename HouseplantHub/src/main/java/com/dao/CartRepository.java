package com.dao;



import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import com.model.Cart;


@Repository
public interface CartRepository extends JpaRepository<Cart, Integer>{
	@Query("from Cart c where c.UserId = :UserId")
	List<Cart> getDataByEmail(@Param("UserId") String UserId);
	
    
}
