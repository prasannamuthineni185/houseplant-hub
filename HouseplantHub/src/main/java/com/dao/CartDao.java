package com.dao;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.model.Product;

@Service
public class CartDao {
	
	@Autowired
	CartRepository cr;
	@Autowired
	ProductRepository pr;
	
	public Product addToCart(int productId){
	Product p=pr.findById(productId);
	return p;
	
	}
}
