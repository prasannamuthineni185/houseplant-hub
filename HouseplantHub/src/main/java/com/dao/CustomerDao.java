package com.dao;

import java.time.LocalDateTime;
import java.util.HashMap;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.mail.SimpleMailMessage;
import org.springframework.mail.javamail.JavaMailSender;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.stereotype.Service;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PutMapping;

import com.model.Customer;
import com.model.CustomerOtp;
import com.twilio.rest.api.v2010.account.Message;
import com.twilio.type.PhoneNumber;

@Service
public class CustomerDao {

	@Autowired
	CustomerRepository customerRepository;
	@Autowired
	JavaMailSender mailSender;
	@Autowired
	CustomerOtp customerOtp;
	HashMap<String,String> Hm = new HashMap<String, String>();
	public List<Customer> getCustomers(){
		return customerRepository.findAll();
	}

	public Customer getCustomerByName(String fname,String lname){
		return customerRepository.findByName(fname,lname);
	}
	public Customer deleteByName(String fname,String lname){
		return customerRepository.deleteByName(fname,lname);
	}
	

	public void registerCustomer(Customer customer){
		BCryptPasswordEncoder bcpe = new BCryptPasswordEncoder();
		String encryptPwd = bcpe.encode(customer.getPassword());
		customer.setPassword(encryptPwd);
		customerRepository.save(customer);
	}

	public void updateCustomer(Customer customer){
		BCryptPasswordEncoder bcpe = new BCryptPasswordEncoder();
		String encryptPwd = bcpe.encode(customer.getPassword());
		customer.setPassword(encryptPwd);
		customerRepository.save(customer);
	}


	public Customer getCustomerByEmailId(String emailId){
		return customerRepository.findByEmailId(emailId);
	}
	public Customer cLogin(String emailId,String password){
		return customerRepository.cLogin(emailId,password);
	}

	public void generateEmailOtp(Customer cust){
		int otp = (int) (Math.random() * 900000) + 100000;
		LocalDateTime expiryTime = LocalDateTime.now().plusMinutes(1);
		customerOtp.setEmailOtp(otp);
		customerOtp.setEmailOtpExpiryTime(expiryTime);
		SimpleMailMessage message = new SimpleMailMessage();
		message.setTo(cust.getEmailId());
		message.setSubject("Your OTP Code");
		message.setText("Your OTP code is " + otp);

		mailSender.send(message);
	}
	public boolean CheckOtp(String otp, String email){
		if (Hm.containsKey(email)){
			if (Hm.get(email).equals(otp)){
				Hm.remove(email);
				return true;
			}
		}
		return false;
	}
	public void AddHm (String email, String Otp){
		System.out.println("hear"+email+" "+Otp);
		Hm.put(email, Otp);
	}
	public void generateEmailOtp2(String email){
		int otp = (int) (Math.random() * 900000) + 100000;
		LocalDateTime expiryTime = LocalDateTime.now().plusMinutes(1);
		customerOtp.setEmailOtp(otp);
		customerOtp.setEmailOtpExpiryTime(expiryTime);
		SimpleMailMessage message = new SimpleMailMessage();
		message.setTo(email);
		message.setSubject("Your OTP Code");
		message.setText("Your OTP code  for forget password is " + otp);
		mailSender.send(message);
		Hm.put(email, String.valueOf(otp));
	}
	public boolean validateEmailOtp(String emailId, int otp) {
		Customer cust = customerRepository.findByEmailId(emailId);
		if(cust != null){
			if(customerOtp.getEmailOtp() == otp && customerOtp.getEmailOtpExpiryTime().isAfter(LocalDateTime.now())){
				customerOtp.setEmailOtp(0);
				customerOtp.setEmailOtpExpiryTime(null);
				return true;
			}
		}
		return false;
	}

	public Customer getStudentByPhoneNo(String phoneNo) {
		return customerRepository.findByPhoneNo(phoneNo);
	}
	
//	//email existing check for forget password
//		@GetMapping("CheckEmail/{email}")
//		public boolean CheckFP(@PathVariable("email") String email){
//			List<Customer> Cd = customerRepository.findAll();
//			for (Customer C:Cd){
//				if (C.getEmailId().equals(email)){
//					CustomerDao.generateEmailOtp2(email);
//					return  true;
//				}
//			}
//			return false;
//		}
//		//if above true make the change of password by retriving both pass and otp at a time
//		@PutMapping("UpdatePass/{email}/{otp}/{password}")
//		public ResponseEntity<String> UpdatePass(@PathVariable("otp") String Otp,@PathVariable("email") String email,@PathVariable("password") String Password){
//			if (CustomerDao.CheckOtp(Integer.parseInt(Otp), email)){
//				return ResponseEntity.ok("Successfull");
//			}
//			return ResponseEntity.ok("unSuccesfull");	
//		}

	public void generatePhoneOtp(Customer cust) {
		int otp = (int) (Math.random() * 9000) + 1000;
		LocalDateTime expiryTime = LocalDateTime.now().plusMinutes(1);
		customerOtp.setPhoneOtp(otp);
		customerOtp.setPhoneOtpExpiryTime(expiryTime);
		Message.creator(new PhoneNumber(cust.getPhoneNo()), new PhoneNumber("+19374513949"), "Hello " + cust.getFirstName()+ ", Your Student OTP is : " + otp + "\n\nOTP generated By Co Plants \nFrom HousePlantHub ").create();
	}

	public boolean validatePhoneOtp(String phoneNo, int otp) {

		Customer stud = customerRepository.findByPhoneNo(phoneNo);
		if(stud != null){
			if(customerOtp.getPhoneOtp() == otp && customerOtp.getPhoneOtpExpiryTime().isAfter(LocalDateTime.now())){
				customerOtp.setPhoneOtp(0);
				customerOtp.setPhoneOtpExpiryTime(null);
				return true;
			}
		}
		return false;
	}

	

}