//package com.dao;
//
//import org.springframework.data.jpa.repository.JpaRepository;
//import org.springframework.data.jpa.repository.Query;
//import org.springframework.data.repository.query.Param;
//import org.springframework.stereotype.Repository;
//
//import com.model.Seeds;
//
//@Repository
//public interface SeedsRepository extends JpaRepository<Seeds, Integer>{
//
//	@Query("from Seeds s where s.SeedsName = :SeedsName")
//	Seeds findByName(@Param("SeedsName") String SeedsName);
//	
//}