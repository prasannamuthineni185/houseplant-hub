//package com.project.HouseplantHub;
//
//import java.io.IOException;
//import java.util.List;
//
//import org.springframework.beans.factory.annotation.Autowired;
//import org.springframework.web.bind.annotation.DeleteMapping;
//import org.springframework.web.bind.annotation.GetMapping;
//import org.springframework.web.bind.annotation.PathVariable;
//import org.springframework.web.bind.annotation.PostMapping;
//import org.springframework.web.bind.annotation.PutMapping;
//import org.springframework.web.bind.annotation.RequestBody;
//import org.springframework.web.bind.annotation.RequestParam;
//import org.springframework.web.bind.annotation.RestController;
//import org.springframework.web.multipart.MultipartFile;
//
//import com.dao.SeedsDao;
//import com.dao.SeedsRepository;
//import com.model.Seeds;
//
//@RestController
//public class SeedsController {
//
//	@Autowired
//	SeedsDao SeedsDao;
//	@Autowired
//	SeedsRepository SR;
//	
//	@GetMapping("getSeeds")
//	public List<Seeds> getSeeds() {
//		return SeedsDao.getSeeds();
//	}
//	
//	@GetMapping("getSeedsById/{SeedsId}")
//	public Seeds getSeedsById(@PathVariable("SeedsId") int SeedsId) {
//		return SeedsDao.getSeedsById(SeedsId);
//	}
//@PostMapping("Seeds/{Seedsname}/{Seedsprice}")
//public String Seeds (@RequestParam("file") MultipartFile file, @PathVariable("Seedsname") String Seedsname, @PathVariable("Seedsprice") double Seedsprice) throws IOException{
//	Seeds sd= new Seeds(Seedsname,Seedsprice,file.getBytes());
//	SR.save(sd);
//		return "success"; 
//	}
//	@GetMapping("getSeedsByName/{SeedsName}")
//	public Seeds getSeedsByName(@PathVariable("SeedsName") String SeedsName) {
//		return SeedsDao.getSeedsByName(SeedsName);
//		
//	}
//	
//	@PostMapping("registerSeeds")
//	public String registerSeeds(@RequestBody Seeds Seeds) {
//		SeedsDao.registerSeeds(Seeds);
//		return "Seed Registered Successfully!!!";
//	}
//	
//	@PutMapping("updateSeeds")
//	public String updateSeeds(@RequestBody Seeds Seeds) {
//		SeedsDao.updateSeeds(Seeds);
//		return "Seed Updated Successfully!!!";
//}
//	
//	@DeleteMapping("deleteSeedsById/{SeedsId}")
//public String deleteSeedsById(@PathVariable("SeedsId") int SeedsId) {
//		SeedsDao.deleteSeedsById(SeedsId);
//	return "Seed Deleted Successfully!!!";
//}
//}