package com.project.HouseplantHub;
import java.util.UUID;
import java.util.ArrayList;
import java.util.List;

import javax.mail.MessagingException;
import javax.mail.internet.MimeMessage;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;

import org.springframework.mail.javamail.JavaMailSender;
import org.springframework.mail.javamail.MimeMessageHelper;

import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import com.dao.CartRepository;
import com.dao.OrderDetailsRepo;
import com.dao.CustomerDao;
import com.dao.CustomerRepository;
import com.dao.ProductRepository;
import com.dao.UserUniqueIdRepo;
import com.model.Cart;
import com.model.CartQ;
import com.model.Customer;
import com.model.OrderDetails;
import com.model.Product;
import com.model.UserUniqueId;
@CrossOrigin("http://localhost:4200")
@RestController
public class CustomerController {
	@Autowired
    private JavaMailSender javaMailSender;
	@Autowired 
	ProductRepository productrep;
	@Autowired 
	OrderDetailsRepo ctqrp;
	@Autowired
	CustomerDao customerDao;
	@Autowired
	CartRepository cartrp;
	@Autowired
	UserUniqueIdRepo useruniqueidrp;
	@Autowired
	CustomerRepository customerRepository;
	@GetMapping("getCustomers")
	public List<Customer> getCustomers(){
		return customerDao.getCustomers();
	}

	@GetMapping("getCustomerByName/{firstName}/{lastName}")
	public Customer getCustomerByName(@PathVariable("firstName") String firstName,@PathVariable("lastName") String lastName){
		return customerDao.getCustomerByName(firstName,lastName);
	}
    
	@PostMapping("registerCustomer")//register
	public String registerCustomer(@RequestBody Customer cust){
		List<Customer> custdetails=  customerRepository.findAll();
		for( Customer cd:custdetails){
			if (cd.getEmailId().equals(cust.getEmailId())){
				return "Existing User";
			}
		}
		UUID uuid = UUID.randomUUID();
		String uuidval=uuid.toString();
		String email=cust.getEmailId();
		UserUniqueId uc= new UserUniqueId(email,uuidval);
		useruniqueidrp.save(uc);
		customerDao.registerCustomer(cust);
		return "Customer Registered Successfully !!!";
		
	}
	@SuppressWarnings("deprecation")
	@GetMapping("retrivecart/{email}")
	public List<CartQ> getproducts(@PathVariable("email") String email){
		UserUniqueId uc= useruniqueidrp.getDataByEmail(email);
		List<Cart> ct = cartrp.getDataByEmail(uc.getUniqueid());
		List<CartQ> dt= new ArrayList<CartQ>();
		
		
		for(Cart c:ct){
			Product pd=productrep.findById(c.getProductId());
			
			
			
			dt.add(new CartQ(pd.getProductId(),pd.getProductName(),pd.getPrice(),pd.getImg(),c.getQuantity()));
			
		}
		return dt;
	}
	//email existing check for forget password
	@GetMapping("CheckEmail/{email}")
	public boolean CheckFP(@PathVariable("email") String email){
		System.out.println(email);
		List<Customer> Cd = customerRepository.findAll();
		for (Customer C:Cd){
			if (C.getEmailId().equals(email)){
				String subject="verifiation otp for authetication ";
				int otp = (int) (Math.random() * 900000) + 100000;
				String body=String.valueOf(otp);
				 MimeMessage message = javaMailSender.createMimeMessage();
				 MimeMessageHelper helper;
			    try {
			        helper = new MimeMessageHelper(message, true);
			        helper.setTo(email);
			        helper.setSubject(subject);
			        helper.setText(body, true);
			        javaMailSender.send(message);
			       
			    } catch (MessagingException e) {
			        e.printStackTrace();
			    }
			    customerDao.AddHm(email, body);
				return  true;
			}
		}
		return false;
	}
	
	
//	//if above true make the change of password by retriving both pass and otp at a time
//	@PutMapping("UpdatePass/{email}/{otp}/{password}")
//	public ResponseEntity<String> UpdatePass(@PathVariable("otp") String Otp,@PathVariable("email") String email,@PathVariable("password") String Password){
//		if (CustomerDao.CheckOtp(Integer.parseInt(Otp), email)){
//			return ResponseEntity.ok("Successfull");
//		}
//		return ResponseEntity.ok("unSuccesfull");	
//	}
	
	
	//if above true make the change of password by retriving both pass and otp at a time
	@PutMapping("UpdatePass/{email}/{otp}/{password}")
	public boolean UpdatePass(@PathVariable("otp") String Otp,@PathVariable("email") String email,@PathVariable("password") String Password){
		System.out.println(Otp);
		if (customerDao.CheckOtp(Otp, email)){
			System.out.println(Otp);
			List<Customer> Cd = customerRepository.findAll();
			for (Customer C:Cd){
				if (C.getEmailId().equals(email)){
				
					C.setPassword(Password);
					customerRepository.save(C);
				}
			}
			return true;
		}
		return false;	
	}
	
	@PutMapping("updateCustomer")
	public String updateCustomer(@RequestBody Customer cust){
		customerDao.updateCustomer(cust);
		return "Customer Updated Successfully !!!";
	}

	@DeleteMapping("deleteCustomerByName/{firstName}/{lastName}")
	public String deleteCustomerByName(@PathVariable("firstName") String firstName,@PathVariable("lastName") String lastName){
		customerDao.deleteByName(firstName,lastName);
		return "Customer Deleted Successfully !!!";
	}
	
	@GetMapping("cLogin/{emailId}/{password}")
	public String cLogin(@PathVariable("emailId") String emailId, @PathVariable("password") String password){
		Customer c = customerDao.getCustomerByEmailId(emailId);
		if(c != null){
			BCryptPasswordEncoder bcpe = new BCryptPasswordEncoder();
			if(bcpe.matches(password, c.getPassword())){
				return "Logined Succesfully";
			}
		}
		return "UnSuccesfully";
	}

	@PostMapping("EmailOtp/{emailId}")
	public String EmailOtp(@PathVariable("emailId") String emailId){
		Customer stud = customerDao.getCustomerByEmailId(emailId);
		if(stud != null){
			customerDao.generateEmailOtp(stud);
			return "OTP sent to : " + emailId;
		}
		return "Email Not Found";
	}

	@PostMapping("PhoneOtp/{PhoneNo}")
	public String otpPhoneNo(@PathVariable("PhoneNo") String phoneNo){
		Customer stud = customerDao.getStudentByPhoneNo(phoneNo);
		if(stud != null){
			customerDao.generatePhoneOtp(stud);
			return "OTP sent to : " + phoneNo;
		}
		return "PhoneNumber not Found";
	}

	@PostMapping("validateEmailOtp/{emailId}/{otp}")
	public String validateEmailOtp(@PathVariable("emailId") String emailId, @PathVariable("otp") int otp){
		if(customerDao.validateEmailOtp(emailId, otp)){
			return "OTP is valid !!!";
		}
		else {
			return "OTP is Invalid !!!";
		}
	}
	@DeleteMapping("deleteCart/{email}")
	public void deleteCart(@PathVariable("email") String email){
		System.out.println("email"+email);
		UserUniqueId uuiddata= useruniqueidrp.getDataByEmail(email);
		List<Cart> cart = cartrp.getDataByEmail(uuiddata.getUniqueid());
		for (Cart c:cart){
			System.out.println(c.getId()+"vknskbjvs");
			cartrp.deleteById(c.getId());
		}
		

	}
	
	@GetMapping("getorder/{order}")
	public List<OrderDetails> getOrder(@PathVariable("order") String order){
		System.out.print("fvndfkjnbv");
		List<OrderDetails> od = ctqrp.getByEmailId(order);
		return od;
	}
	@PostMapping("order/{email}")
	public void Order(@RequestBody List<CartQ> order, @PathVariable("email") String email){
		for(CartQ c:order){
			
			OrderDetails od=new OrderDetails(c.getProductId(), c.getProductName(),c.getPrice(),c.getImg(),c.getQuantity(),email);
			ctqrp.save(od);
		}
		
	}
	@PostMapping("usercart/{emailid}/{productid}")
	public void UserCart(@PathVariable("emailid") String emailid,@PathVariable("productid") int productid ){
		UserUniqueId uuiddata= useruniqueidrp.getDataByEmail(emailid);
		System.out.println("hello"+uuiddata.getUniqueid()+uuiddata.getEmail());
		List<Cart> ct = cartrp.findAll();
		
		int count=0;
		for (Cart c: ct){
			if (c.getUserId().equals(uuiddata.getUniqueid()) && c.getProductId()==productid){
				
				System.out.println("inside");
				count=count+1;
				int val= c.getQuantity();
				val=val+1;
				c.setQuantity(val);
				cartrp.save(c);
			}
		}
		if (count==0){
			System.out.println("outside");
			Cart c= new Cart(uuiddata.getUniqueid(),productid,1);
			cartrp.save(c);
		}
		
		
		
	}

	@PostMapping("validatePhoneOtp/{PhoneNo}/{otp}")
	public String validatePhoneOtp(@PathVariable("PhoneNo") String phoneNo, @PathVariable("otp") int otp){
		if(customerDao.validatePhoneOtp(phoneNo, otp)){
			return "OTP is valid !!!";
		}
		else {
			return "OTP is Invalid !!!";
		}
	}

}