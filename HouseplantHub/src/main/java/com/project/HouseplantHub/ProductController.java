package com.project.HouseplantHub;

import java.io.IOException;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.multipart.MultipartFile;

import com.dao.ProductDao;
import com.dao.ProductRepository;
import com.model.Product;

@RestController
public class ProductController {

	@Autowired
	ProductDao productDao;
	@Autowired
	ProductRepository PR;
	
	@GetMapping("getProducts")
	public List<Product> getProducts() {
		return productDao.getProducts();
	}
	
	@GetMapping("geProductById/{productId}")
	public Product getProductById(@PathVariable("productId") int productId) {
		return productDao.getProductById(productId);
	}
@PostMapping("plants/{productname}/{productprize}")
public String Products (@RequestParam("file") MultipartFile file, @PathVariable("productname") String productname, @PathVariable("productprize") double productprize) throws IOException{
	Product pd= new Product(productname,productprize,file.getBytes());
	PR.save(pd);
		return "success"; 
	}
//	@GetMapping("getProductByName/{productName}")
//	public Product getProductByName(@PathVariable("productName") String productName) {
//		return productDao.getProductByName(productName);
//		
//	}
	
	@PostMapping("registerProduct")
	public String registerProduct(@RequestBody Product product) {
		productDao.registerProduct(product);
		return "Product Registered Successfully!!!";
	}
	
	@PutMapping("updateProduct")
	public String updateProduct(@RequestBody Product product) {
		productDao.updateProduct(product);
		return "Product Updated Successfully!!!";
}
	
	@DeleteMapping("deleteProductById/{productId}")
public String deleteProductById(@PathVariable("productId") int productId) {
		productDao.deleteProductById(productId);
	return "Product Deleted Successfully!!!";
}
}