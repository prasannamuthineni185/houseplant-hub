package com.model;

//import java.util.Date;

import javax.persistence.Entity;

import javax.persistence.Id;
import javax.persistence.Column;


@Entity
public class Customer {
	@Id
	 @Column(unique = true, nullable = false)
	private String firstName;
	private String lastName;
	private String emailId;
	private String password;
	private String PhoneNo;
    
	public Customer() {
		super();
	}
	
	public Customer(String firstName,String lastName, String emailId,
			String password, String PhoneNo) {
		super();
		this.firstName = firstName;
		this.lastName = lastName;
		this.emailId = emailId;
		this.password = password;
		this.PhoneNo = PhoneNo;
	}
	
	public String getFirstName() {
		return firstName;
	}
	
	public void setFirstName(String firstName) {
		this.firstName = firstName;
	}
	
	public String getLastName() {
		return lastName;
	}
	
	public void setLastName(String lastName) {
		this.lastName = lastName;
	}
	
	public String getEmailId() {
		return emailId;
	}
	
	public void setEmailId(String emailId) {
		this.emailId = emailId;
	}
	
	public String getPassword() {
		return password;
	}
	
	public void setPassword(String password) {
		this.password = password;
	}
	
	public String getPhoneNo() {
		return PhoneNo;
	}
	
	public void setPhoneNo(String PhoneNo) {
		this.PhoneNo = PhoneNo;
	}
	
}