package com.model;

import javax.persistence.Entity;


public class CartQ {
	private int productId;
	private String productName;
    private double price;
    private byte[] img;
    private int quantity;
    
	public CartQ() {
		super();
	}

	public CartQ(int productId, String productName, double price, byte[] img, int quantity) {
		super();
		this.productId = productId;
		this.productName = productName;
		this.price = price;
		this.img = img;
		this.quantity = quantity;
	}

	public int getProductId() {
		return productId;
	}

	public void setProductId(int productId) {
		this.productId = productId;
	}

	public String getProductName() {
		return productName;
	}

	public void setProductName(String productName) {
		this.productName = productName;
	}

	public double getPrice() {
		return price;
	}

	public void setPrice(double price) {
		this.price = price;
	}

	public byte[] getImg() {
		return img;
	}

	public void setImg(byte[] img) {
		this.img = img;
	}

	public int getQuantity() {
		return quantity;
	}

	public void setQuantity(int quantity) {
		this.quantity = quantity;
	}
}
