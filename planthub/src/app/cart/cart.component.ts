import { Component, OnInit } from '@angular/core';
import { plantService } from '../plant.service';
import { Router } from '@angular/router';
import { NgFor } from '@angular/common';
@Component({
  selector: 'app-cart',
  templateUrl: './cart.component.html',
  styleUrls: ['./cart.component.css']
})
export class CartComponent{
  product:any;
  data:any=0;
  cartItems: any;
  cart:any;
  emailId: any;
  
  constructor(private router: Router,private service: plantService) {
    

  }
  postorder( product:any){
    this.service.orderpost(product);
    this.service.deleteCart();
  }
  ngOnInit(): void {

    this.emailId = localStorage.getItem("emailId");

    console.log(this.emailId);

    this.service.getcart(this.emailId).subscribe((Data: any) => {
      this.product = Data;
      
    for (var i = 0; i < this.product.length; i++) {
      this.data=this.data+this.product[i]['price']*this.product[i]['quantity']
    }
     
    }); 
    
  }





  // incrementQuantity(item: any) {
  //   item.quantity += 1;
  //   this.service.updateCart(item);
  //   this.syncMedicinesWithCart();
  // }

  // decrementQuantity(item: any) {
  //   item.quantity -= 1;
  //   if (item.quantity <= 0) {
  //       this.service.removeFromCart(item);
  //       item.inCart = false;
  //       item.quantity = 1; // Reset to initial quantity if needed
  //   } else {
  //       this.service.updateCart(item);
  //   }
  //   this.syncMedicinesWithCart();
  // }

  // removeFromCart(item: any) {
  //   this.service.removeFromCart(item);
  //   item.inCart = false;
  //   item.quantity = 1; // Reset to initial quantity
  //   this.syncMedicinesWithCart();
  // }

  // getTotalPrice(): number {
  //   return this.cartItems.reduce((total, medicine) => total + (medicine.price * medicine.quantity), 0);
  // }

  // validateMedForm(medForm: any) {

  // }





  deleteCartItem(product: any) {
    const i = this.cartItems.findIndex((element: any) => {
      return element.id == product.id;
    });

    this.cartItems.splice(i, 1);
  }

}