import { Injectable, inject } from '@angular/core';
import { ActivatedRouteSnapshot, CanActivateFn, RouterStateSnapshot } from '@angular/router';
import { plantService } from './plant.service';


// @Injectable({providedIn:'root'})
// export class AuthGuard {

//   constructor(private service: plantService) {
//   }

//   canActivate(route: ActivatedRouteSnapshot, state: RouterStateSnapshot): boolean {
//     //return true;
//     return this.service.getUserLoggedStatus();
//   }  
// }


export const authGuard: CanActivateFn = (route, state) => {
  let service = inject(plantService);
  return service.getUserLoggedStatus();
};
