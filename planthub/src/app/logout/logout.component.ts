import { Component } from '@angular/core';
import { plantService } from '../plant.service';
import { Router } from '@angular/router';

@Component({
  selector: 'app-logout',
  templateUrl: './logout.component.html',
  styleUrls: ['./logout.component.css']
})
export class LogoutComponent {

  constructor(private router: Router, private service: plantService) {
    this.service.setUserLoggedOut();
    router.navigate(['login']);
  }
}
