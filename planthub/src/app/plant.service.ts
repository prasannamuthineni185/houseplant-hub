import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import axios from 'axios';
import { Subject } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class plantService {

  isUserLogged: boolean;

  //Dependency Injection for HTTPClient
  loginStatus: Subject<any>;
  response: any;
  otpresponse: any;
  emailId:any;
  constructor(private http: HttpClient) {
    this.isUserLogged = false;
    this.loginStatus = new Subject();
    this.cartItems = [];                                            
  }

  getLoginStatus() {
    return this.loginStatus.asObservable();
  }
   async CheckOtp(email:any){
     await axios.get(`CheckEmail/`+email).then(data=>{
        console.log(data.data,"getrdxh");
        return data.data
      })
  }
  async PutPass(Email: any, otp: any, Npass: any){
    await axios.put(`http://localhost:4200/UpdatePass/`+Email+`/`+otp +`/`+Npass ).then(data=>{
      console.log(data.data,"getrdxh");
      return data.data
    })
  }
  setUserLoggedIn() {
    this.isUserLogged = true;
    this.loginStatus.next(true);
  }
  setUserLoggedOut() {
    this.isUserLogged = false;
    this.loginStatus.next(false);
  }
  getUserLoggedStatus(): boolean {
    return this.isUserLogged;
  }

  getCustomers(): any {
    return this.http.get('http://localhost:8085/getCustomers');
  }

  getCustomerByName(cName: any): any {
    return this.http.get('http://localhost:8085/getCustomerByName/' + cName);
  }

  customerRegister(customer: any) {
    return this.http.post('registerCustomer', customer);
  }
  getProducts(): any {
    return this.http.get('getProducts');
  }

  getSeeds(): any {
    return this.http.get('getSeeds');
  }

   async cLogin(customer: any) {
    await axios.get('cLogin/' + customer.emailId + "/" + customer.password).then(data=>{
      console.log(data.data,"getrdxh");
      return data.data
    })
    //return this.http.get('cLogin/' + customer.emailId + "/" + customer.password).toPromise();
  }

  updateCust(employee: any) {
    return this.http.put('updateCustomer', employee);
  }

  deleteCust(empId: any) {
    return this.http.delete('deleteCustomerByName/' + empId);
  }
  
  getcart(email:any) {
    return this.http.get('retrivecart/'+ email);
  }

  cartItems: any;

  

  addToCart(product: any) {
    this.emailId = localStorage.getItem("emailId");
    axios.post(`http://localhost:8085/usercart/`+this.emailId+`/`+product);
  }
  getOrderItems() {
    this.emailId = localStorage.getItem("emailId");
    return this.http.get('http://localhost:8085/getorder/'+this.emailId)
  }

  orderpost(product:any){
   this.emailId = localStorage.getItem("emailId");
   axios.post('http://localhost:8085/order/'+this.emailId,product);
   
  }

  deleteCart(){
    this.emailId = localStorage.getItem("emailId");
    console.log("in delete");
    axios.delete('http://localhost:8085/deleteCart/'+this.emailId);
    console.log("in delete 2");
  }

}