import { Component, OnInit } from '@angular/core';
import { plantService } from '../plant.service';

@Component({
  selector: 'app-products',
  templateUrl: './products.component.html',
  styleUrls: ['./products.component.css']
})
export class ProductsComponent implements OnInit{

  product: any;
emailId:any;
  constructor(private service:plantService) {
  
  }
  
  ngOnInit(): void {
      this.service.getProducts().subscribe((prodData: any) => {
        this.product = prodData;
        console.log(prodData);
      }); 
  }

  addToCart(val:any) {
    console.log("calling cart");
    this.emailId = localStorage.getItem("emailId");
    console.log(val,"vv")
    this.service.addToCart(val.productId);
  }
}