import { Component, OnInit } from '@angular/core';
import { plantService } from '../plant.service';

@Component({
  selector: 'app-header',
  templateUrl: './header.component.html',
  styleUrls: ['./header.component.css']
})
export class HeaderComponent implements OnInit {

  loginStatus: any;
  //cartItems: any;

  constructor(private service: plantService) {
    //this.cartItems = service.cartItems;
  }

  ngOnInit() {
    this.service.getLoginStatus().subscribe((data: any) => {
      this.loginStatus = data;
    });

    
  
  }

  getUserLoggedStatus(){
    return this.service.getUserLoggedStatus();
  }

}

