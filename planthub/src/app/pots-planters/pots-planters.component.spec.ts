import { ComponentFixture, TestBed } from '@angular/core/testing';

import { PotsPlantersComponent } from './pots-planters.component';

describe('PotsPlantersComponent', () => {
  let component: PotsPlantersComponent;
  let fixture: ComponentFixture<PotsPlantersComponent>;

  beforeEach(() => {
    TestBed.configureTestingModule({
      declarations: [PotsPlantersComponent]
    });
    fixture = TestBed.createComponent(PotsPlantersComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
