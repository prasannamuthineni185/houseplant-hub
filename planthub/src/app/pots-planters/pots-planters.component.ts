import { Component, OnInit } from '@angular/core';
import {planters} from '../dummyData/plantersData'
@Component({
  selector: 'app-pots-planters',
  templateUrl: './pots-planters.component.html',
  styleUrls: ['./pots-planters.component.css']
})
export class PotsPlantersComponent implements OnInit {
    data:any;

    constructor(){}
    
  
  ngOnInit(): void {
    console.log("planters",planters)
    this.data=planters;
  }
}
