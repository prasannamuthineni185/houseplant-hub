import { Component } from '@angular/core';
import { Router } from '@angular/router';
import { plantService } from '../plant.service';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css']
})
export class LoginComponent {
  customer: any;

  //Dependency Injection for Router, EmpService
  constructor(private router: Router, private service: plantService) {

    //Delete All the Employee Hardcoded JSON objects

  }

  async validateLogin(loginForm: any) {
    this.customer = null;
    console.log(loginForm);

    //Implementing LocalStorage
    localStorage.setItem("emailId", loginForm.emailId);

    if (loginForm.emailId == "Harshitha" && loginForm.password == "Harshitha") {
      this.service.setUserLoggedIn();
      this.router.navigate(['adminheader']);

    } else {

      await this.service.cLogin(loginForm).then((data: any) => {
        console.log(data);
        this.customer = data;
      });

      if (this.customer = "Succesfull" ) {
        this.service.setUserLoggedIn();
        this.router.navigate(['home']);
      } else {
        alert('Invalid Credentials');
      }
    }
  }

}
