import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { LoginComponent } from './login/login.component';
import { RegisterComponent } from './register/register.component';
import { HeaderComponent } from './header/header.component';
import { HomeComponent } from './home/home.component';
import { ProductsComponent } from './products/products.component';
import { LogoutComponent } from './logout/logout.component';
import { authGuard } from './auth.guard';
import { PotsPlantersComponent } from './pots-planters/pots-planters.component';
import { BlogComponent } from './blog/blog.component';
import {ForgotPasswordComponent} from './forgot-password/forgot-password.component'
import { CartComponent } from './cart/cart.component';
import { OurstoryComponent } from './ourstory/ourstory.component';
import { AdminheaderComponent } from './adminheader/adminheader.component';
import { OrdersComponent } from './orders/orders.component';



//import { AuthGuard } from './auth.guard';
const routes: Routes = [
  {path:"",        component:HomeComponent},
  {path:"login",       component:LoginComponent},
  {path:"register",    component:RegisterComponent},
  {path:"forgotPassword",    component:ForgotPasswordComponent},
  {path:"header",      component:HeaderComponent},
  {path:"products",    canActivate:[authGuard], component:ProductsComponent},
  {path:"logout",      canActivate:[authGuard], component:LogoutComponent},
  {path:"pots-planters",component:PotsPlantersComponent },
  {path:"home",component:HomeComponent},
  {path:"blog",component:BlogComponent},
  {path:"cart",component:CartComponent},
  {path:"ourstory",component:OurstoryComponent},
  {path:"adminheader",component:AdminheaderComponent},
  {path:"orders",component:OrdersComponent}


  

];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }