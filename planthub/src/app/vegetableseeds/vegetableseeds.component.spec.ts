import { ComponentFixture, TestBed } from '@angular/core/testing';

import { VegetableseedsComponent } from './vegetableseeds.component';

describe('VegetableseedsComponent', () => {
  let component: VegetableseedsComponent;
  let fixture: ComponentFixture<VegetableseedsComponent>;

  beforeEach(() => {
    TestBed.configureTestingModule({
      declarations: [VegetableseedsComponent]
    });
    fixture = TestBed.createComponent(VegetableseedsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
