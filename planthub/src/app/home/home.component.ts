import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.css']
})
export class HomeComponent implements OnInit {
  images: any = [
    {
      imageDesciption: "Haruhi Suzumiya",
      image: "../../assets/images/background_1.webp"
        
    },
    {
      imageDesciption: "Yuki Nagato",
      image:
       "../../assets/images/background_2.webp"
    },
    
  ];
  actualImage: string | undefined;
  changeBackgroundCounter = 1;
  constructor() { }

  getImages() {
    console.log("addddd")
    return this.images.slice();
  }

  ngOnInit() {
    // let userinfoList: any = sessionStorage.getItem('loginData');
    // console.log("plantproject------>",userinfoList)
    this.images = this.getImages();
    this.actualImage = this.images[0].image;
    setInterval(() => {
      this.changeBackgroundCounter++;
      if (this.changeBackgroundCounter > this.images.length - 1) {
        this.changeBackgroundCounter = 1;
      }
      this.actualImage = this.images[this.changeBackgroundCounter].image;
    }, 1000);
  }

}



