import { Component, OnInit } from '@angular/core';
import { plantService } from '../plant.service';

@Component({
  selector: 'app-orders',
  templateUrl: './orders.component.html',
  styleUrls: ['./orders.component.css']
})
export class OrdersComponent implements OnInit {
  product:any;

  constructor(private service:plantService){


  }
  ngOnInit(): void {
    this.service.getOrderItems().subscribe((prodData: any)=>{
      this.product=prodData;
    })
      

  }



}
