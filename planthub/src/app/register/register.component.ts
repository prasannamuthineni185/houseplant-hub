import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { plantService } from '../plant.service';

@Component({
  selector: 'app-register',
  templateUrl: './register.component.html',
  styleUrls: ['./register.component.css']
})
export class RegisterComponent implements OnInit {

  customer: any;

  //Dependency Injection for plantService
  constructor(private router: Router, private service: plantService) {
    this.customer = {
      "firstName": "",
      "lastName":"",
      "emailId": "",
      "password": "",
      "PhoneNo":""
      }
  }

  ngOnInit(){
   
  }

  customerRegister(regForm: any) {

    console.log(regForm);

    this.customer.firstName = regForm.firstName;
    this.customer.lastName = regForm.lastName;
    this.customer.emailId = regForm.emailId;
    this.customer.password = regForm.password;
    this.customer.PhoneNo = regForm.PhoneNo;

    this.service.customerRegister(this.customer).subscribe((data: any) => {
      if(data==="Existing User"){
        alert("This email already exits")

      }
      else{
          alert('Registered Successfully!');
          console.log(data);
          this.router.navigate(['login']);
          
      }
      
    });
  }
  showLogin() {
    this.router.navigate(['login']);
  }

}