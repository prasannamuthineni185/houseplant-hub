import { Component, OnInit } from '@angular/core';
import { plantService } from '../plant.service';

@Component({
  selector: 'app-seeds',
  templateUrl: './seeds.component.html',
  styleUrls: ['./seeds.component.css']
})
export class SeedsComponent  implements OnInit{

  seed: any;

  constructor(private service:plantService) {
  
  }
  ngOnInit(): void {
      this.service.getSeeds().subscribe((seedsData: any) => {
        this.seed = seedsData;
        console.log(seedsData);
      }); 
  }
}
