import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { LoginComponent } from './login/login.component';
import { RegisterComponent } from './register/register.component';
import { LogoutComponent } from './logout/logout.component';
import { HeaderComponent } from './header/header.component';
import { FormsModule } from '@angular/forms';
import { RouterModule } from '@angular/router';
import { HttpClientModule } from '@angular/common/http';
import { ProductsComponent } from './products/products.component';
import { FlowerseedsComponent } from './flowerseeds/flowerseeds.component';
import { HomeComponent } from './home/home.component';
import { ScrollbarComponent } from './scrollbar/scrollbar.component';
import { BestsellersComponent } from './bestsellers/bestsellers.component';
import { PotsPlantersComponent } from './pots-planters/pots-planters.component';
import { BlogComponent } from './blog/blog.component';
import { ForgotPasswordComponent } from './forgot-password/forgot-password.component';
import { FooterComponent } from './footer/footer.component';
import { SeedsComponent } from './seeds/seeds.component';
import { CartComponent } from './cart/cart.component';
import { OurstoryComponent } from './ourstory/ourstory.component';
import { AdminheaderComponent } from './adminheader/adminheader.component';
import { Bestsellers1Component } from './bestsellers1/bestsellers1.component';
import { XlPlantsComponent } from './xl-plants/xl-plants.component';
import { VegetableseedsComponent } from './vegetableseeds/vegetableseeds.component';
import { OrdersComponent } from './orders/orders.component';


@NgModule({
  declarations: [
    AppComponent,
    LoginComponent,
    RegisterComponent,
    LogoutComponent,
    HeaderComponent,
    ProductsComponent,
    FlowerseedsComponent,
    HomeComponent,
    ScrollbarComponent,
    BestsellersComponent,
    PotsPlantersComponent,
    BlogComponent,
    ForgotPasswordComponent,
    FooterComponent,
    SeedsComponent,
    CartComponent,
    OurstoryComponent,
    AdminheaderComponent,
    Bestsellers1Component,
    XlPlantsComponent,
    VegetableseedsComponent,
    OrdersComponent,
  
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    FormsModule,
    RouterModule,
    HttpClientModule
  ],

  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
